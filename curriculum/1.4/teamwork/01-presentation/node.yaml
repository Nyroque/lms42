name: Presentation & self improvement
goals:
    presentation: 1
    personalmanagement: 1
days: 2
allow_longer: true
type: presentation
upload: false
public: users
avg_attempts: 1
resources:
    -
        link: https://www.youtube.com/watch?v=BmEiZadVNWY
        title: TEDxEindhoven - How to present to keep your audience’s attention
        topics: MLK, ditch powerpoint, ask questions, tell stories
    -
        link: https://www.youtube.com/watch?v=K0pxo-dS9Hc
        title: TEDxZagreb - The 110 techniques of communication and public speaking
        topics: body language, pace, pauses, uhh.
    -
        link: https://www.smallgroups.com/articles/2016/how-can-i-lead-great-small-group-discussions.html
        title: How Can I Lead Great Small-Group Discussions?

assignment: 
- |
    <div class="notification is-warning">Please note that this assignment counts as part of the module exam.</div> 

    Do a 15 to 20 minute presentation in front of the class (during a lunch break) on any topic that may positively affect the health, happiness, productivity or wealth of software developers. As part of your talk you should reflect upon the topic at hand with regard to your personal circumstances.

    After your talk, you're expected to lead a 15m discussion on the topic, which you should prepare beforehand. It might be a good idea to have some questions or statements available to get the discussion going.
    
    Topics that have already been covered by a classmate within the last year cannot be recycled. If you need inspiration, here's a list of examples:

    Health:
    - programming posture
    - ergonomic keyboard and keyboard layouts
    - how not to RSI
    - recognizing depression early on
    - vitamins
    - healthy body weight
    - being a software developer with ADHD
    - being a software developer with autism
    - being a software developer with dyslexia
    - alcohol and software development
    - how and why to sleep
    - biohacking

    Productivity:
    - how to enter the zone/flow
    - the 7 habits of highly effective people
    - pomodoro
    - how to focus
    - personal knowledge bases
    - work/life balance
    - procrastination
    - motivation hacking

    Social:
    - how to dress for nerds
    - how to make friends
    - who's opinion should you care about
    - privacy: why and how
    - influencing people

    Happiness:
    - what factors are important to happiness
    - which jobs make people happy
    - which hobbies make people happy
    - quarterlife crisis
    - setting (life) goals

    Money:
    - negotiating your salary
    - how not to spend too much
    - startup: what, why and how
    - how to start a company
    - how to fund a startup
    - coming up with startup ideas
    - bookkeeping 101
    - investing
    - (not) buying status symbols

    Talk to your teacher about scheduling your presentation. You may use a beamer, whiteboard, flip over or any other tools. Ask your teacher if you have any requirements not present in the classroom.
    
    Note that you will be cut off after 20 minutes.

-
    title: Content
    map:
        personalmanagement: 1
    text: Well-researched, coherent and novel for most of the target audience.
    0: Chaotic, highly questionable and superficial.
    4: Well-researched, coherent, relevant and novel for most of the target audience.
-
    title: Presentation
    map:
        presentation: 2
    text: Clear, confident, engaging and connecting with the audience.
    0: Hesitant, unintelligible and dreadfully boring.
    4: Clear, confident, engaging and connecting with the audience.
-
    title: Discussion
    map:
        presentation: 1
    text: Encourage many people in the audience to voice their thoughts. Try to find differences of opinion and delve deeper into what causes them. Gently lead the discussion back onto the topic at hand when it moves sideways.
    2: Some clear attempts were made to do what is described in the objective, though it didn't really work out.
    4: As described in the objective, resulting in a interesting discussion actively involving at least 5 audience members.


-
    link: https://www.youtube.com/watch?v=GP1ww_1AJzI
    title: "How to Give Constructive and Actionable Peer Feedback: Students to Students"

-
    title: Feedback
    must: true
    text: |
        For the next two presentations for this assignment by classmates, send the presenter an email with your detailed feedback and tips on her/his presentation, with a *cc* to the day's teacher. The goal is to help them improve, while passively training your own presentation skills as well.

        You don't need to delay submitting this assignment until you have provided this feedback. We assume that you will do this to the best of your abilities, when these presentation happen. (And if not, we can always retract your grade.)
