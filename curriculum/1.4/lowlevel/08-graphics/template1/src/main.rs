const WIDTH: usize = 900;
const HEIGHT: usize = 900;


struct Image {
    width: usize,
    height: usize,
    buffer: Vec<u32>,
}


impl Image {
    /// Load a PNG-file as an `Image`.
    fn load(filename: &str) -> Image {
        let image = lodepng::decode32_file(&filename).expect(&format!("Couldn't load {}", &filename));
        Image {
            width: image.width,
            height: image.height,
            buffer: image.buffer.iter().map(|color| {
                // TODO:
                //   `color` has red (`r`), green (`g`), blue (`b`) and alpha (`a`) u8 values. They
                //   should be returned as a single u32, where alpha is bits 24...31 (the most significant
                //   bits), red is bits 16...23, green is bits 8...15 and blue is bits 0...8 (the least
                //   significant bits).
                0x11223344
            }).collect(),
        }
    }
}


fn main() {
    // Load the two images
    let image1 = Image::load("mainframe1.png");
    let image2 = Image::load("mainframe2.png");

    // Create a window
    let mut window = minifb::Window::new(
        "Rotating images",
        WIDTH as usize,
        HEIGHT as usize,
        minifb::WindowOptions {
            resize: false,
            topmost: true,
            scale: minifb::Scale::FitScreen,
            scale_mode: minifb::ScaleMode::AspectRatioStretch,
            ..minifb::WindowOptions::default()
        },
    ).expect("Unable to create window");

    // The output buffer that we'll render the output image to
    let mut buffer = vec![0u32; (WIDTH*HEIGHT) as usize];

    // The current rotation in radians (0..2*Pi)
    let mut rotation = 0.0;

    // Whether we are just drawing a single image (0), rotating a single image (1),
    // or rotating two superimposed images in different directions (2).
    let mut mode = 0;
    
    // Variables to keep track of frames per second
    let mut fps_time = std::time::Instant::now();
    let mut fps_count = 0;

    // The main loop
    while window.is_open() && !window.is_key_down(minifb::Key::Escape) {        

        // Rotate a bit
        rotation += 0.01;

        // Change the mode
        if window.is_key_pressed(minifb::Key::Space, minifb::KeyRepeat::No) {
            mode = (mode+1) % 3;
        }

        // Draw!
        if mode==0 {
            // TODO: just draw a single image
        } else if mode==1 {
            // TODO: rotate a single image
        } else {
            // TODO: rotate two superimposed images in different directions
            // To prevent code duplication, you may want to move your code from
            // your `mode==1` answer into a function or a method of `Image`.
        }

        // Show the buffer in the window
        window.update_with_buffer(&buffer, WIDTH as usize, HEIGHT as usize).unwrap();

        // Display the FPS rate 3 times per second
        let now = std::time::Instant::now();
        let duration = (now - fps_time).as_secs_f32();
        fps_count += 1;
        if duration >= 0.33 {
            println!("{} fps", (fps_count as f32 / duration).round() as u32);
            fps_time = now;
            fps_count = 0;
        }
    }
}
