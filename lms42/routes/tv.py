from ..app import db, app
import flask

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    return flask.render_template('tv.html', header=False, device=device)
